// imanu - Basic in-browser image manipulation
// Copyright (c) 2017 Hagen Schnuch

/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */


/* Greasemonkey info */
// ==UserScript==
// @name        imanu
// @namespace   imanu
// @description Basic in-browser image manipulation.
// @include     *.bmp*
// @include     *.gif*
// @include     *.jpe*
// @include     *.jpg*
// @include     *.png*
// @include     *.tif*
// @version     1
// @grant       none
// ==/UserScript==



// Wait until everything (the image) is loaded.
// Only needed/working when the image is embeded in a website, so
// uncomment this when using the script outside of greasemonkey/webextension.
//
window.onload = function() 
{

// ===== Preview enabled/disabled by default =====
var preview_on_off = 1;	// [0/1]



/* ======= style ======= */
var style = document.createElement("style");
style.innerHTML = `
    .panel { 
		width:200px; height:100%; position:fixed; right:0; z-index:99; overflow:auto;
        font-family:Ubuntu,Arial,Helvetica,sans-serif;
		color:#ccc; background:#2f343f; background:hsla(210,10%,10%,0.6);
		border-left:1px solid #3f454f;
		transition:transform 0.25s, opacity 0.25s; opacity:0.2; transform:translate(150px,0);
	}
    .panel:hover { opacity:1; transform:translate(0,0); }

    .panel > div { width:80%; margin:20px auto; position:relative; }
    .panel > div > input[type=range] { margin:5px 0; width:100%; }
    .panel > div > span[id*=reset] { font-size:1.4em; line-height:1rem; float:right; }
    .panel > div > span[id*=reset]:hover { cursor:pointer; }

	#preview_div > *, #crop_div > * { vertical-align:middle; }
	
	canvas { 
		display:none;
		margin:auto; position:absolute;
		left:0; right:0; top:0; bottom:0;
	}

	#save_div { text-align:center; }
	#save_a { 
		color:hsl(210,30%,60%)!important; background:hsla(210,10%,10%,0.2);
		border:1px solid hsl(210,30%,60%); border-radius:3px;
		padding:0.2em 0.5em; text-decoration:none; opacity:0.8;
	}
	#save_a:hover { opacity:1; }

	#hint_div { font-size:0.8em; }
`;
document.head.appendChild(style);
/* ------- style ------- */




var filterValues = [];
var image = document.getElementsByTagName("img")[0];

/* ========== canvas ========== */
var canvas = document.createElement("canvas");
canvas.id = "canvas";
document.body.appendChild(canvas);

var image_nw = image.naturalWidth;
var image_nh = image.naturalHeight;
var image_ar = image_nw/image_nh;

canvas.width = image.width;
canvas.height = image.height;

var context = canvas.getContext("2d");
context.imageSmoothingQuality = "high";
context.drawImage( image, 0,0, canvas.width,canvas.height );

var dataURL = canvas.toDataURL("image/jpeg");


// Set style if preview is enabled by default.
if( preview_on_off ) {
	image.style.display = "none";
	canvas.style.display = "block";
}



/* ===== apply filters ===== */
function writeValues() {
	if( preview_check.checked ){
		context.filter = "none";
		for( var i=0; i < filterValues.length; i++ ){
			if( context.filter == "none" ) context.filter = filterValues[i];
			else context.filter += filterValues[i];
		}
		//context.clearRect( 0,0, canvas.width,canvas.height );		// not needed for now
		context.drawImage( image, 0,0, canvas.width,canvas.height );
		dataURL = canvas.toDataURL("image/jpeg");
		save_a.href = dataURL;
	}
}



/* ========== panel ========== */
var panel = document.createElement("div");
panel.id = "panel";
panel.className = "panel";
document.body.appendChild(panel);

context.filter = "blur(5px) ";


/* ======= preview ======= */
var preview_div = document.createElement("div");
preview_div.id = "preview_div";

var preview_check = document.createElement("input");
preview_check.id = "preview_check";
preview_check.type = "checkbox";
preview_check.checked = preview_on_off;

var preview_label = document.createElement("span");
preview_label.id = "preview_label";
//preview_label.textContent = "Vorschau";	// DE
preview_label.textContent = "Preview";	// EN

preview_div.appendChild(preview_check);
preview_div.appendChild(preview_label);
panel.appendChild(preview_div);

preview_check.onchange = function() {
	if( preview_check.checked ){
		image.style.display = "none";
		canvas.style.display = "block";
		writeValues();
	} else {
		image.style.display = "block";
		canvas.style.display = "none";
	}
}


/* ===== saturate ===== */
var saturate_default = 1;
var saturate_div = document.createElement("div");
saturate_div.id = "saturate_div";

var saturate_label = document.createElement("span");
saturate_label.id = "saturate_label";
//saturate_label.textContent = "Sättigung: ";	// DE
saturate_label.textContent = "Saturation: ";	// EN
var saturate_value = document.createElement("span");
saturate_value.id = "saturate_value";
saturate_value.textContent = "1";
var saturate_reset = document.createElement("span");
saturate_reset.id = "saturate_reset";
saturate_reset.innerHTML = "&#x27f2;";

var saturate_range = document.createElement("input");
saturate_range.id = "saturate_range";
saturate_range.type = "range";
saturate_range.min = 0;
saturate_range.max = 2;
saturate_range.value = saturate_default;
saturate_range.step = 0.05;

saturate_div.appendChild(saturate_label);
saturate_div.appendChild(saturate_value);
saturate_div.appendChild(saturate_reset);
saturate_div.appendChild(saturate_range);
panel.appendChild(saturate_div);

function saturateChange() {
    saturate_value.textContent = saturate_range.value;
	filterValues[0] = "saturate(" + saturate_range.value + ") ";
	writeValues();
}
saturate_range.onchange = saturateChange;
saturate_range.oninput = function() { saturate_value.textContent = saturate_range.value; }
saturate_reset.onclick = function() {
	saturate_range.value = saturate_default;
	saturateChange();
}


/* ===== contrast ===== */
var contrast_default = 1;
var contrast_div = document.createElement("div");
contrast_div.id = "contrast_div";

var contrast_label = document.createElement("span");
contrast_label.id = "contrast_label";
//contrast_label.textContent = "Kontrast: ";	// DE
contrast_label.textContent = "Contrast: ";	// EN
var contrast_value = document.createElement("span");
contrast_value.id = "contrast_value";
contrast_value.textContent = "1";
var contrast_reset = document.createElement("span");
contrast_reset.id = "contrast_reset";
contrast_reset.innerHTML = "&#x27f2;";

var contrast_range = document.createElement("input");
contrast_range.id = "contrast_range";
contrast_range.type = "range";
contrast_range.min = 0;
contrast_range.max = 2;
contrast_range.value = contrast_default;
contrast_range.step = 0.05;

contrast_div.appendChild(contrast_label);
contrast_div.appendChild(contrast_value);
contrast_div.appendChild(contrast_reset);
contrast_div.appendChild(contrast_range);
panel.appendChild(contrast_div);

function contrastChange() {
    contrast_value.textContent = contrast_range.value;
	filterValues[1] = "contrast(" + contrast_range.value + ") ";
	writeValues();
}
contrast_range.onchange = contrastChange;
contrast_range.oninput = function() { contrast_value.textContent = contrast_range.value; }
contrast_reset.onclick = function() {
	contrast_range.value = contrast_default;
	contrastChange();
}


/* ===== brightness ===== */
var brightness_default = 1;
var brightness_div = document.createElement("div");
brightness_div.id = "brightness_div";

var brightness_label = document.createElement("span");
brightness_label.id = "brightness_label";
//brightness_label.textContent = "Helligkeit: ";	// DE
brightness_label.textContent = "Brightness: ";	// EN
var brightness_value = document.createElement("span");
brightness_value.id = "brightness_value";
brightness_value.textContent = "1";
var brightness_reset = document.createElement("span");
brightness_reset.id = "brightness_reset";
brightness_reset.innerHTML = "&#x27f2;";

var brightness_range = document.createElement("input");
brightness_range.id = "brightness_range";
brightness_range.type = "range";
brightness_range.min = 0;
brightness_range.max = 2;
brightness_range.value = brightness_default;
brightness_range.step = 0.05;

brightness_div.appendChild(brightness_label);
brightness_div.appendChild(brightness_value);
brightness_div.appendChild(brightness_reset);
brightness_div.appendChild(brightness_range);
panel.appendChild(brightness_div);

function brightnessChange() {
	brightness_value.textContent = brightness_range.value;
	filterValues[2] = "brightness(" + brightness_range.value + ") ";
	writeValues();
}
brightness_range.onchange = brightnessChange;
brightness_range.oninput = function() { brightness_value.textContent = brightness_range.value; }
brightness_reset.onclick = function() {
	brightness_range.value = 1;
	brightnessChange();
}


/* ===== sepia ===== */
var sepia_default = 0;
var sepia_div = document.createElement("div");
sepia_div.id = "sepia_div";

var sepia_label = document.createElement("span");
sepia_label.id = "sepia_label";
sepia_label.textContent = "Sepia: ";
var sepia_value = document.createElement("span");
sepia_value.id = "sepia_value";
sepia_value.textContent = "0";
var sepia_reset = document.createElement("span");
sepia_reset.id = "sepia_reset";
sepia_reset.innerHTML = "&#x27f2;";

var sepia_range = document.createElement("input");
sepia_range.id = "sepia_range";
sepia_range.type = "range";
sepia_range.min = 0;
sepia_range.max = 1;
sepia_range.value = sepia_default;
sepia_range.step = 0.05;

sepia_div.appendChild(sepia_label);
sepia_div.appendChild(sepia_value);
sepia_div.appendChild(sepia_reset);
sepia_div.appendChild(sepia_range);
panel.appendChild(sepia_div);

function sepiaChange() {
    sepia_value.textContent = sepia_range.value;
	filterValues[3] = "sepia(" + sepia_range.value + ") ";
	writeValues();
}
sepia_range.onchange = sepiaChange;
sepia_range.oninput = function() { sepia_value.textContent = sepia_range.value; }
sepia_reset.onclick = function() {
	sepia_range.value = sepia_default;
	sepiaChange();
}


/* ===== invert ===== */
var invert_default = 0;
var invert_div = document.createElement("div");
invert_div.id = "invert_div";

var invert_label = document.createElement("span");
invert_label.id = "invert_label";
//invert_label.textContent = "Negativ: ";	// DE
invert_label.textContent = "Negative: ";	// EN
var invert_value = document.createElement("span");
invert_value.id = "invert_value";
invert_value.textContent = "0";
var invert_reset = document.createElement("span");
invert_reset.id = "invert_reset";
invert_reset.innerHTML = "&#x27f2;";

var invert_range = document.createElement("input");
invert_range.id = "invert_range";
invert_range.type = "range";
invert_range.min = 0;
invert_range.max = 1;
invert_range.value = invert_default;
invert_range.step = 1;

invert_div.appendChild(invert_label);
invert_div.appendChild(invert_value);
invert_div.appendChild(invert_reset);
invert_div.appendChild(invert_range);
panel.appendChild(invert_div);

function invertChange() {
    invert_value.textContent = invert_range.value;
	filterValues[4] = "invert(" + invert_range.value + ") ";
	writeValues();
}
invert_range.onchange = invertChange;
invert_range.oninput = function() { invert_value.textContent = invert_range.value; }
invert_reset.onclick = function() {
	invert_range.value = 0;
	invertChange();
}


/* ===== hue ===== */
var hue_default = 0;
var hue_div = document.createElement("div");
hue_div.id = "hue_div";

var hue_label = document.createElement("span");
hue_label.id = "hue_label";
//hue_label.textContent = "Tönung: ";	// DE
hue_label.textContent = "Hue: ";	// EN
var hue_value = document.createElement("span");
hue_value.id = "hue_value";
hue_value.textContent = "0";
var hue_reset = document.createElement("span");
hue_reset.id = "hue_reset";
hue_reset.innerHTML = "&#x27f2;";

var hue_range = document.createElement("input");
hue_range.id = "hue_range";
hue_range.type = "range";
hue_range.min = 0;
hue_range.max = 360;
hue_range.value = hue_default;
hue_range.step = 1;

hue_div.appendChild(hue_label);
hue_div.appendChild(hue_value);
hue_div.appendChild(hue_reset);
hue_div.appendChild(hue_range);
panel.appendChild(hue_div);

function hueChange() {
    hue_value.textContent = hue_range.value;
	filterValues[5] = "hue-rotate(" + hue_range.value + "deg) ";
	writeValues();
}
hue_range.onchange = hueChange;
hue_range.oninput = function() { hue_value.textContent = hue_range.value; }
hue_reset.onclick = function() {
	hue_range.value = hue_default;
	hueChange();
}


/* ===== blur ===== */
var blur_default = 0;
var blur_div = document.createElement("div");
blur_div.id = "blur_div";

var blur_label = document.createElement("span");
blur_label.id = "blur_label";
//blur_label.textContent = "Unschärfe*: ";	// DE
blur_label.textContent = "Blur*: ";			// EN
var blur_value = document.createElement("span");
blur_value.id = "blur_value";
blur_value.textContent = "0";
var blur_reset = document.createElement("span");
blur_reset.id = "blur_reset";
blur_reset.innerHTML = "&#x27f2;";

var blur_range = document.createElement("input");
blur_range.id = "blur_range";
blur_range.type = "range";
blur_range.min = 0;
blur_range.max = 100;
blur_range.value = blur_default;
blur_range.step = 1;

blur_div.appendChild(blur_label);
blur_div.appendChild(blur_value);
blur_div.appendChild(blur_reset);
blur_div.appendChild(blur_range);
panel.appendChild(blur_div);

function blurChange() {
    blur_value.textContent = blur_range.value;
	filterValues[6] = "blur(" + blur_range.value + "px) ";
	writeValues();
}
blur_range.onchange = blurChange;
blur_range.oninput = function() { blur_value.textContent = blur_range.value; }
blur_reset.onclick = function() {
	blur_range.value = blur_default;
	blurChange();
}



/* ===== verkleinern ===== */
var crop_div = document.createElement("div");
crop_div.id = "crop_div";

var crop_check = document.createElement("input");
crop_check.id = "crop_check";
crop_check.type = "checkbox";
crop_check.checked = 0;

var crop_label = document.createElement("span");
crop_label.id = "crop_label";
//crop_label.textContent = "Originalgröße";	// DE
crop_label.textContent = "Original size";		// EN

crop_div.appendChild(crop_check);
crop_div.appendChild(crop_label);
panel.appendChild(crop_div);

crop_check.onchange = resizeCanvas;



/* ========== download ========== */
var save_div = document.createElement("div");
save_div.id = "save_div";
panel.appendChild(save_div);
var save_a = document.createElement("a");
save_a.id = "save_a";
save_a.textContent = "Download";
save_div.appendChild(save_a);

save_a.href = dataURL;
save_a.download = "edited-image.jpg";



/* ===== Hinweise ===== */
var hint_div = document.createElement("div");
hint_div.id = "hint_div";
panel.appendChild(hint_div);

// DE
//hint_div.innerHTML = `
//	Das Bild wird durch klicken auf "Download" in der angezeigten Größe heruntergeladen.
//	<br/>
//	Das Original kann mit deaktivierter Vorschau(!) per Rechtsklick auf das Bild gespeichert werden.
//	<br/>
//	<br/>
//	*Damit die Unschärfe am Rand richtig angewendet wird, muss der Regler einige Male verstellt werden.
//	`;

// EN
hint_div.innerHTML = `
	When clicking on "Download", the image will be downloaded in the shown size.
	<br/>
	Right-clicking on the image - with disabled preview(!) - allows downloading the original file.
	<br/>
	<br/>
	*To accurately show blur on the edges, you need to change the blur value multiple times.
	`;



/* ===== window resize ===== */
window.onresize = resizeCanvas;

function resizeCanvas() {
	if( crop_check.checked ){
		if( window.innerWidth < image_nw ) canvas.style.bottom = "auto";
		if( window.innerHeight < image_nh ) canvas.style.bottom = "auto";
		canvas.width = image_nw;
		canvas.height = image_nh;
	} else {
		if( canvas.style.bottom != "" ) canvas.style.bottom = "";
		if( canvas.style.right != "" ) canvas.style.right = "";

		if( window.innerWidth/window.innerHeight < image_ar ) {
			canvas.width = window.innerWidth;
			canvas.height = canvas.width / image_ar;
		} else {
			canvas.height = window.innerHeight;
			canvas.width = canvas.height * image_ar;
		}
		//canvas.width = image.width;
		//canvas.height = image.height;
	}
	if( window.innerWidth > image_nw && window.innerHeight > image_nh ) {
		if( canvas.style.bottom != "" ) canvas.style.bottom = "";
		if( canvas.style.right != "" ) canvas.style.right = "";
		canvas.width = image_nw;
		canvas.height = image_nh;
	}
	writeValues();
}


} // main window.onload function ends here




/* ==========   TODO   ==========
 *
 * - save as jpg/png
 * - auto save file when done processing - deprecated
 * - use original size / shrink - DONE
 * - crop
 * - faster processing ? - deprecated
 * - show processing time - deprecated
 * - show range values while changing - DONE
 *
 * - other processing features
 *
 * - maybe add predefined filters
 *
 */
