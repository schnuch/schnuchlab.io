/* --- Datum & Uhrzeit ---*/
showdatetime();
setInterval("showdatetime()", 1000);

function showdatetime() {
    document.getElementById("clocky").innerHTML = datey() +" - "+ clocky();
}


/*  Uhrzeit */
function clocky() {
    var now = new Date();
    var hh = now.getHours();
    var mm = now.getMinutes();
 	//var ss = now.getSeconds();
    if (hh < 10) { hh = "0"+ hh; }
    if (mm < 10) { mm = "0"+ mm; }
 	//if (ss < 10) { ss = "0"+ ss; }
    //var timey = hh +":"+ mm +" Uhr";
    var timey = hh +":"+ mm;
    return timey;
}


/*  Datum */
function datey() {
  var now = new Date();
  //var dayNames = new Array("Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag");
  var dayNames = new Array("Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday");
  //var monNames = new Array("Januar","Februar","März","April","Mai","June","Juli","August","September","Oktober","November","Dezember");
  //var monNames = new Array("Jan","Feb","Mär","Apr","Mai","Jun","Jul","Aug","Sep","Okt","Nov","Dez");
  var monNames = new Array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
  var datey = dayNames[now.getDay()] +",  "+ now.getDate() +".  "+ monNames[now.getMonth()] +". "+ now.getFullYear();
  return datey;
}
